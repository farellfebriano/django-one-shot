from django import forms
from todos.models import TodoList

class TodoListCreate(forms.ModelForm):
    class Meta:
        model = TodoList
        fields = [
            'name',
            "id"
        ]
