from django import forms
from todos.models import TodoList

class Todo_List_Updata(forms.ModelForm):
    class Meta:
        model = TodoList
        fields = (
            "name"
        )
