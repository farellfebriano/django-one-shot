# Generated by Django 4.2.1 on 2023-05-31 20:16

import datetime
from django.db import migrations, models


class Migration(migrations.Migration):
    initial = True

    dependencies = []

    operations = [
        migrations.CreateModel(
            name="TodoList",
            fields=[
                (
                    "id",
                    models.BigAutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                ("name", models.CharField(max_length=100)),
                (
                    "created_on",
                    models.TimeField(
                        verbose_name=datetime.datetime(
                            2023,
                            5,
                            31,
                            20,
                            16,
                            5,
                            784275,
                            tzinfo=datetime.timezone.utc,
                        )
                    ),
                ),
            ],
        ),
    ]
