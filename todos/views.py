from django.shortcuts import render, get_object_or_404,redirect
from todos.models import TodoList

# Create your views here.


def todolist(request):
    form = TodoList.objects.all()
    context = {"todo_list": form}
    return render(request, "todo_list.html", context)


def todo_list_detail(request,id):
    form = get_object_or_404(TodoList,id=id)
    context = {"todo_list_detail": form}
    return render(request, "todo_list_detail.html", context)

from todos.todo_list_create import TodoListCreate

def todo_list_create(request):
    if request.method == "POST":
        form = TodoListCreate(request.POST)
        if form.is_valid():
            list = form.save()
            return redirect("todo_list_detail", id=list.id )


    form = TodoListCreate()
    context ={
        "create_todo_list":form,
    }
    return render(request,"create_todo_list.html",context)

from todos.todo_list_create import TodoListCreate

def todo_list_update(request,id):
    post = get_object_or_404(request.POST,id)
    if request.method =="POST":
        form = TodoListCreate(request.POST, instance=post)
        if form.is_valid():
            form_object = form.save()
            return redirect("todo_list_detail", id=form_object.id)
    form = TodoListCreate()
    context = {
        "form":form
    }
    return render(request,"todo_list_update.html",context)

#cara gua

# def todo_list_delet(request,id):
#     form = get_object_or_404(TodoList,id=id)
#     if request.method == "POST":
#         form.delete()
#         return redirect("todo_list_list")
#     return render(request, "todo_list_delet.html",context=None)

#cara lain

def todo_list_delet(request,id):
    form = TodoList.objects.get(id=id)
    if request.method == "POST":
        form.delete()
        return redirect("todo_list_list")
    return render(request, "todo_list_delete.html", context=None)



from todos.todo_item_create import TodoItemCreate
def todo_item_create(request):
    if request.method == "POST":
        form = TodoItemCreate(request.POST)
        if form.is_valid():
            item = form.save()
            return redirect("todo_list_detail",id=item.list.id)
    form = TodoItemCreate()
    context={
        "todo_list_detail":form
    }
    return render(request, "todo_item_create.html",context)
