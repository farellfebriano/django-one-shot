from django import forms
from todos.models import TodoItem
class TodoItemCreate(forms.ModelForm):
    class Meta:
        model = TodoItem
        fields =(
            "task",
            "due_date",
            "is_completed",
            "list",
        )
