# Generated by Django 4.2.1 on 2023-05-31 20:26

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("todos", "0004_alter_todolist_created_on"),
    ]

    operations = [
        migrations.AlterField(
            model_name="todolist",
            name="created_on",
            field=models.DateTimeField(auto_now_add=True),
        ),
    ]
